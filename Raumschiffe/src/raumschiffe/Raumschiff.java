package raumschiffe;

import java.util.ArrayList;
import java.util.List;

public class Raumschiff {
	// Deklaration der Instanzvariablen
	private String raumschiffName;
	private int energieversorgungProzent, schutzschildeProzent, lebenserhaltungssystemeProzent, huelleProzent,
			photonentorpedosAnzahl, reparaturAndroidenAnzahl;
	static List<Ladung> ladungsverzeichnis = new ArrayList<>();
	/**
	 * blabla
	 *
	 */
	static List<String> broadcastKommunikator = new ArrayList<>();

	// Konstruktoren
	Raumschiff() {
	}

	Raumschiff(String raumschiffName, int energieversorgungProzent, int schutzschildeProzent,
			int lebenserhaltungssystemeProzent, int huelleProzent, int photonentorpedosAnzahl,
			int reparaturAndroidenAnzahl, ArrayList<Ladung> ladungsverzeichnis) {
		setRaumschiffName(raumschiffName);
		setEnergieversorgungProzent(energieversorgungProzent);
		setSchutzschildeProzent(schutzschildeProzent);
		setLebenserhaltungssystemeProzent(lebenserhaltungssystemeProzent);
		setHuelleProzent(huelleProzent);
		setPhotonentorpedosAnzahl(photonentorpedosAnzahl);
		setReparaturAndroidenAnzahl(reparaturAndroidenAnzahl);
		setLadungsverzeichnis(ladungsverzeichnis);
	}

	// Verwaltungsmethoden (Getter & Setter)
	public void setRaumschiffName(String raumschiffName) {
		this.raumschiffName = raumschiffName;
	}

	public String getRaumschiffName() {
		return this.raumschiffName;
	}

	public void setEnergieversorgungProzent(int energieversorgungProzent) {
		this.energieversorgungProzent = energieversorgungProzent;
	}

	public int getEnergieversorgungProzent() {
		return this.energieversorgungProzent;
	}

	public void setSchutzschildeProzent(int schutzschildeProzent) {
		this.schutzschildeProzent = schutzschildeProzent;
	}

	public int getSchutzschildeProzent() {
		return this.schutzschildeProzent;
	}

	public void setLebenserhaltungssystemeProzent(int lebenserhaltungssystemeProzent) {
		this.lebenserhaltungssystemeProzent = lebenserhaltungssystemeProzent;
	}

	public int getLebenserhaltungssystemeProzent() {
		return this.lebenserhaltungssystemeProzent;
	}

	public void setHuelleProzent(int huelleProzent) {
		this.huelleProzent = huelleProzent;
	}

	public int getHuelleProzent() {
		return this.huelleProzent;
	}

	public void setPhotonentorpedosAnzahl(int photonentorpedosAnzahl) {
		this.photonentorpedosAnzahl = photonentorpedosAnzahl;
	}

	public int getPhotonentorpedosAnzahl() {
		return this.photonentorpedosAnzahl;
	}

	public void setReparaturAndroidenAnzahl(int reparaturAndroidenAnzahl) {
		this.reparaturAndroidenAnzahl = reparaturAndroidenAnzahl;
	}

	public int getReparaturAndroidenAnzahl() {
		return this.reparaturAndroidenAnzahl;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		Raumschiff.ladungsverzeichnis = ladungsverzeichnis;
	}

	public List<Ladung> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}

	// Methoden
	public void zustandAusgeben() {

		System.out.println("+++++++++ RAUMSCHIFFSCHIFFSZUSTAND +++++++++");
		System.out.println("Raumschiffname: " + raumschiffName);
		System.out.println("Energieversorgung in %: " + energieversorgungProzent);
		System.out.println("Schutzschilde in %: "+ schutzschildeProzent);
		System.out.println("Lebenserhaltungssysteme in %: " + lebenserhaltungssystemeProzent);
		System.out.println("Huelle in %: " + huelleProzent);
		System.out.println("Anzahl der Photonentorpedos: " + photonentorpedosAnzahl);
		System.out.println("Anzahl der Reparaturandroiden: " + reparaturAndroidenAnzahl);
		System.out.println("Anzahl der Ladungen: " + ladungsverzeichnis.size());

	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("+++++++++ LADUNGSVERZEICHNIS +++++++++");
		System.out.println("Raumschiffname: " + raumschiffName);

		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println("Ladungsbezeichnung: " + ladungsverzeichnis.get(i).getBezeichnung() + " / Menge: " + ladungsverzeichnis.get(i).getMenge());
		}
	}

	public void photonentorpedoAbschiessen(Raumschiff ziel) {
		if (photonentorpedosAnzahl < 1) {
			nachrichtVersenden("-=*Click*=-");
		} else {
			photonentorpedosAnzahl -= 1;
			nachrichtVersenden("Photonentorpedo abgeschossen!");
			ziel.trefferVermerken(true, 100);
		}
	}

	public void phaserkanonenAbschiessen(Raumschiff ziel) {
		if (energieversorgungProzent < 50) {
			nachrichtVersenden("-=*Click*=-");
		} else {
			setEnergieversorgungProzent(energieversorgungProzent - 50);
			nachrichtVersenden("Phaserkanone abgeschossen!");
			ziel.trefferVermerken(true, 100);
		}
	}

	private void trefferVermerken(boolean treffer, int ausmass) {
		System.out.println(raumschiffName + " wurde getroffen!");

		setSchutzschildeProzent(schutzschildeProzent - 50);

		if (schutzschildeProzent == 0) {
			setHuelleProzent((huelleProzent - 50));
			setEnergieversorgungProzent(energieversorgungProzent - 50);

			if (huelleProzent == 0) {
				setLebenserhaltungssystemeProzent(0);
				nachrichtVersenden("Lebenserhaltungssysteme zerstört!");
			}
		}
	}

	public void nachrichtVersenden(String nachricht) {
		broadcastKommunikator.add(raumschiffName + ": " + nachricht);
	}

	public static List<String> logbuchEintraege() {
		return broadcastKommunikator;
	}

	public void torpedorohreBeladen(int anzahl) {

	}

	public void reparaturAuftragSenden(boolean checkSchutzschilde, boolean checkLebenserhaltungssysteme,
			boolean checkHuellle, int eingesetzteAndroiden) {

	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
}