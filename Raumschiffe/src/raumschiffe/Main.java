package raumschiffe;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Raumschiff Klingonen = new Raumschiff();

		Klingonen.setRaumschiffName("IKS Hegh'ta");
		Klingonen.setPhotonentorpedosAnzahl(1);
		Klingonen.setEnergieversorgungProzent(100);
		Klingonen.setSchutzschildeProzent(100);
		Klingonen.setHuelleProzent(100);
		Klingonen.setLebenserhaltungssystemeProzent(100);
		Klingonen.setReparaturAndroidenAnzahl(2);

		Klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		Klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

		Raumschiff Romulaner = new Raumschiff();

		Romulaner.setRaumschiffName("IRW Kharaza");
		Romulaner.setPhotonentorpedosAnzahl(2);
		Romulaner.setEnergieversorgungProzent(100);
		Romulaner.setSchutzschildeProzent(100);
		Romulaner.setHuelleProzent(100);
		Romulaner.setLebenserhaltungssystemeProzent(100);
		Romulaner.setReparaturAndroidenAnzahl(2);

		Romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		Romulaner.addLadung(new Ladung("Rote Materie", 2));
		Romulaner.addLadung(new Ladung("Plasma Waffe", 50));

		Raumschiff Vulkanier = new Raumschiff();

		Vulkanier.setRaumschiffName("Ni'Var");
		Vulkanier.setPhotonentorpedosAnzahl(0);
		Vulkanier.setEnergieversorgungProzent(80);
		Vulkanier.setSchutzschildeProzent(80);
		Vulkanier.setHuelleProzent(50);
		Vulkanier.setLebenserhaltungssystemeProzent(100);
		Vulkanier.setReparaturAndroidenAnzahl(5);

		Vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		Vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

		Klingonen.photonentorpedoAbschiessen(Romulaner);
		Romulaner.phaserkanonenAbschiessen(Klingonen);

		Vulkanier.nachrichtVersenden("Gewalt ist nicht logisch!");

		Klingonen.zustandAusgeben();
		Klingonen.ladungsverzeichnisAusgeben();

		Klingonen.photonentorpedoAbschiessen(Romulaner);
		Klingonen.photonentorpedoAbschiessen(Romulaner);

		Romulaner.zustandAusgeben();
		Romulaner.ladungsverzeichnisAusgeben();

		Vulkanier.zustandAusgeben();
		Vulkanier.ladungsverzeichnisAusgeben();

	}
}