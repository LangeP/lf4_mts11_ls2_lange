package raumschiffe;


public class Ladung {
	// Deklaration der Instanzvariablen
	private String bezeichnung;
	private int menge;

	// Konstruktoren
	public Ladung() {
	}

	Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}

	// Verwaltungsmethoden
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public int getMenge() {
		return this.menge;
	}

@Override
public String toString(){
	return  this.getClass().getSimpleName() + " " + bezeichnung + " " + menge;
}

}
