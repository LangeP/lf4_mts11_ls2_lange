import java.awt.*;

public class GeometrieTest {


    public static void main(String[] args) {

        Kreis kreis = new Kreis(2.5, Color.WHITE);
        Rechteck rechteck = new Rechteck(2, 4, Color.BLUE);
        Dreieck dreieck = new Dreieck(3, 4, 5, Color.GREEN);

        Figur[] figur = {kreis, rechteck, dreieck};

        Ausgabe(figur);

        kreis.setRadius(5);
        rechteck.setFarbe(Color.DARK_GRAY);
        dreieck.setSeiteC(4);

        Ausgabe(figur);

    } // end of main

    public static void Ausgabe(Figur[] figur) {
        for (int i = 0; i < figur.length; i++) {
            System.out.print(figur[i].toString());
        }
    }


} // end of class program
