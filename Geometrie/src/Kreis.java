import java.awt.*;

public class Kreis extends Figur {

    private double radius;

    public Kreis(double radius, Color farbe) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double umfang() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double flaeche() {
        return 2 * Math.PI * Math.pow(radius, 2);
    }

    @Override
    public String toString() {
        return "Figur: Kreis\n" +
                "Flaeche: " + flaeche() + "\n" +
                "Umfang: " + umfang() + "\n" +
                "Farbe: " + farbe + "\n";
    }
}
