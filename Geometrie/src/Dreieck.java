import java.awt.*;

public class Dreieck extends Figur {

    private double seiteA, seiteB, seiteC;

    public Dreieck(double seiteA, double seiteB, double seite, Color farbe) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.seiteC = seite;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    public double getSeiteC() {
        return seiteC;
    }

    public void setSeiteC(double seiteC) {
        this.seiteC = seiteC;
    }

    public double alpha() {
        return Math.acos((Math.pow(seiteC, 2) + Math.pow(seiteB, 2) - Math.pow(seiteA, 2)) / (2 * seiteB * seiteC));
    }

    public double beta() {
        return Math.acos((Math.pow(seiteA, 2) + Math.pow(seiteC, 2) - Math.pow(seiteB, 2)) / (2 * seiteA * seiteC));
    }

    public double gamma() {
        return Math.acos((Math.pow(seiteA, 2) + Math.pow(seiteB, 2) - Math.pow(seiteC, 2)) / (2 * seiteA * seiteB));
    }

    public double hoeheA() {
        return seiteC * Math.sin(beta());
    }

    public double hoeheB() {
        return seiteC * Math.sin(alpha());
    }

    public double hoeheC() {
        return seiteC * Math.sin(beta());
    }

    @Override
    public double umfang() {
        return seiteA + seiteB + seiteC;
    }

    @Override
    public double flaeche() {
        return 0.5 * (seiteB * seiteC * Math.sin(alpha()));
    }

    @Override
    public String toString() {
        return "Figur: Dreieck\n" +
                "Flaeche: " + flaeche() + "\n" +
                "Umfang: " + umfang() + "\n" +
                "Farbe: " + farbe + "\n";

    }
}
