import java.awt.*;

public abstract class Figur {


    protected Color farbe;

    public Color getFarbe() {
        return farbe;
    }

    public void setFarbe(Color farbe) {
        this.farbe = farbe;
    }

    public abstract double umfang();

    public abstract double flaeche();

    public abstract String toString();


}
