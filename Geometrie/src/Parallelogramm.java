public class Parallelogramm extends Figur {
    private double seiteA, seiteB, alpha;

    public Parallelogramm(double seiteA, double seiteB, double alpha) {
        this.seiteA = seiteA;
        this.seiteB = seiteB;
        this.alpha = alpha;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getSeiteA() {
        return seiteA;
    }

    public void setSeiteA(double seiteA) {
        this.seiteA = seiteA;
    }

    public double getSeiteB() {
        return seiteB;
    }

    public void setSeiteB(double seiteB) {
        this.seiteB = seiteB;
    }

    public double beta() {
        return 180 - alpha;
    }

    public double hoeheA() {
        return seiteB * Math.sin(alpha);
    }

    public double hoeheB() {
        return seiteA * Math.sin(beta());
    }

    public double diagonaleA() {
        return Math.sqrt(Math.pow(seiteA, 2) + Math.pow(seiteB, 2) * 2 * seiteA * seiteB * Math.cos(beta()));
    }

    public double diagonaleB() {
        return Math.sqrt(Math.pow(seiteA, 2) + Math.pow(seiteB, 2) * 2 * seiteA * seiteB * Math.cos(beta()));
    }

    @Override
    public double umfang() {
        return 2 * seiteA + 2 * seiteB;
    }

    @Override
    public double flaeche() {
        return hoeheA() * seiteA;
    }

    @Override
    public String toString() {
        return "Figur: Parallelogramm\n" +
                "Flaeche: " + flaeche() + "\n" +
                "Umfang: " + umfang() + "\n" +
                "Farbe: " + farbe + "\n";
    }
}

